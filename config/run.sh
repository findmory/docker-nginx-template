#!/bin/bash

container_name=$1

if [ -z "$2" ]; then
  port=80
else
  port=$2
fi

eval "$(docker-machine env default)"

docker stop $container_name
docker rm $container_name
docker run --name $container_name -v "$PWD":/usr/share/nginx/html -v "$PWD"/config/nginx.conf:/etc/nginx/nginx.conf:ro -p $port:80 -d nginx


