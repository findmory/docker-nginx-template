DIR=$1

if [ "${DIR:-null}" = null ]; then
	echo "please provide a target path ./copy_to.sh <target>"
else
	cp -R ./config $DIR
fi