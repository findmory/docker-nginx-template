# nginx webhost for dev
## The idea here is that you can quickly spin up an nginx backed container for testing web apps on local dev

### Steps:

+ Be sure default docker-machine is running with:

`docker-machine ls`

+ if you don't see 'default' get it running with:

`docker-machine create default`

+ If machine is there and not running, start with:

`docker-machine start default`

+ now simply run the 'run' script passing in a container name

`./config/run.sh your-container [port]`

Your web page will be hosted on port 80 (or custom port if you specify) of whatever IP your machine is running on

Check with:

`docker-machine ip default`